package com.rednetty.status.commands;

import com.rednetty.status.StatusMain;
import com.rednetty.status.enums.status.StatusEnums;
import com.rednetty.status.mechanics.menus.MenuHandler;
import com.rednetty.status.mechanics.menus.MenuPlayer;
import com.rednetty.status.mechanics.menus.statusmenu.StatusMenu;
import com.rednetty.status.utils.string.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetStatusCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (strings.length > 0 && strings.length < 3) {
            MenuHandler menuHandler = StatusMain.getMechanicHandler().getMenuHandler();
            Player target = Bukkit.getPlayer(strings[0]);
            if (target != null && target.isOnline()) {
                if (commandSender instanceof Player) {
                    Player player = (Player) commandSender;
                    if (strings.length == 1) {
                        MenuPlayer menuPlayer = menuHandler.getMenuPlayer(player);
                        MenuPlayer menuTarget = menuHandler.getMenuPlayer(target);
                        menuPlayer.openMenu(new StatusMenu(menuTarget, true));
                    }
                }
                if (strings.length == 2) {
                    if (StatusEnums.valueOf(strings[1].toUpperCase()) != null) {
                        StatusEnums statusEnums = StatusEnums.valueOf(strings[1].toUpperCase());
                        StatusMain.getMechanicHandler().getStatusMechanic().setStatus(target, statusEnums);
                        commandSender.sendMessage(StringUtil.colorCode("&7Status Updated."));
                    }
                } else {
                    commandSender.sendMessage(StringUtil.colorCode("&cIncorrect use of the command /setstatus <name> <status>!"));
                    return false;
                }
            } else {
                commandSender.sendMessage(StringUtil.colorCode("&cTargeted player is not online!"));
                return false;
            }
        } else {
            commandSender.sendMessage(StringUtil.colorCode("&cIncorrect use of the command /setstatus <name> <status>!"));
            return false;
        }
        return false;
    }
}
