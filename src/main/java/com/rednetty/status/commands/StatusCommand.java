package com.rednetty.status.commands;

import com.rednetty.status.StatusMain;
import com.rednetty.status.mechanics.menus.MenuPlayer;
import com.rednetty.status.mechanics.menus.statusmenu.StatusMenu;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StatusCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof Player) {
            Player player = (Player)commandSender;
            MenuPlayer menuPlayer = StatusMain.getMechanicHandler().getMenuHandler().getMenuPlayer(player);
            menuPlayer.openMenu(new StatusMenu(menuPlayer,false));
            return false;
        }else{
            return true;
        }
    }
}
