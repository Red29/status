package com.rednetty.status;

import com.rednetty.status.commands.SetStatusCommand;
import com.rednetty.status.commands.StatusCommand;
import com.rednetty.status.mechanics.MechanicHandler;
import org.bukkit.plugin.java.JavaPlugin;

public class StatusMain extends JavaPlugin {

    private static StatusMain statusMain;
    private static MechanicHandler mechanicHandler;

    public static StatusMain getInstance() {
        return statusMain;
    }

    /**
     * @return Returns a instance of the MechanicHandler class so that it can be accessed from anywhere.
     */
    public static MechanicHandler getMechanicHandler() {
        return mechanicHandler;
    }

    public void onEnable() {
        statusMain = this;

        /*Registers and Initializes the Mechanic System*/
        mechanicHandler = new MechanicHandler();
        mechanicHandler.init();

        /*Registers Commands*/
        registerCommands();
    }

    public void onDisable() {
        mechanicHandler.stop();
    }



    public void registerCommands() {
        getCommand("SetStatus").setExecutor(new SetStatusCommand());
        getCommand("Status").setExecutor(new StatusCommand());
    }
}
