package com.rednetty.status.mechanics.status;

import com.rednetty.status.enums.status.StatusEnums;
import com.rednetty.status.mechanics.Mechanic;
import com.rednetty.status.utils.string.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.HashMap;

public class StatusMechanic extends Mechanic implements Listener {

    private HashMap<Player, StatusEnums> statusHashMap = new HashMap<>();

    @Override
    public void onEnable() {
        listener(this);
    }

    @Override
    public void onDisable() {

    }

    public HashMap<Player, StatusEnums> getStatusHashMap() {
        return statusHashMap;
    }

    /**
     * Disables hitting when two players have the same Status
     *
     * @param event
     */
    @EventHandler
    public void onHit(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
            Player damager = (Player) event.getDamager();
            Player target = (Player) event.getEntity();
            if (getStatus(damager) == getStatus(target) && getStatus(damager) != StatusEnums.NONE) {
                damager.sendMessage(StringUtil.colorCode("&cYou cannot hit someone with the same status as you!"));
                event.setDamage(0.0);
                event.setCancelled(true);
            }
        } else if (event.getDamager() instanceof Projectile && event.getEntity() instanceof Player) {
            Projectile projectile = (Projectile) event.getDamager();
            if (projectile.getShooter() instanceof Player) {
                Player damager = (Player) projectile.getShooter();
                Player target = (Player) event.getEntity();
                if (getStatus(damager) == getStatus(target) && getStatus(damager) != StatusEnums.NONE) {
                    damager.sendMessage(StringUtil.colorCode("&cYou cannot hit someone with the same status as you!"));
                    event.setDamage(0.0);
                    event.setCancelled(true);
                }
            }
        }
    }


    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        setStatus(event.getEntity(), StatusEnums.NONE);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        setStatus(event.getPlayer(), StatusEnums.NONE);
    }

    /**
     * Used to get the Current Status of Player
     *
     * @param player - Player you're requesting data for
     * @return - Returns Player's Current Status
     */
    public StatusEnums getStatus(Player player) {
        return getStatusHashMap().get(player);
    }


    /**
     * Allows Changing and Editing of a Players Status
     *
     * @param player      - Player that is changing statusEnums
     * @param statusEnums - new Status of the Player
     */
    public void setStatus(Player player, StatusEnums statusEnums) {
        if (statusEnums == StatusEnums.NONE) {
            player.sendMessage(StringUtil.colorCode("&7Status cleared."));
        }else{
            player.sendMessage(StringUtil.colorCode("&7Status changed to " + statusEnums.getDisplayName()));
        }
        player.getPlayer().playSound(player.getPlayer().getLocation(), Sound.ENTITY_ARROW_HIT_PLAYER, 1F, 1F);
        getStatusHashMap().put(player, statusEnums);
        updateStatusDisplay(player, statusEnums);
    }

    public void updateStatusDisplay(Player player, StatusEnums statusEnums) {

        Scoreboard scoreboard;
        if (player.getScoreboard() != null) {
            scoreboard = player.getScoreboard();
        } else {
            scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
            player.setScoreboard(scoreboard);
        }
        if (scoreboard.getObjective("showhealth") == null) {
            scoreboard.registerNewObjective("showhealth", "health");
            Objective obj = scoreboard.getObjective("showhealth");
            obj.setDisplaySlot(DisplaySlot.BELOW_NAME);
            obj.setDisplayName(ChatColor.RED.toString() + "❤");
        }

        Team team;
        if (scoreboard.getTeam(player.getName()) != null) {
            team = scoreboard.getTeam(player.getName());
        } else {
            team = scoreboard.registerNewTeam(player.getName());
        }
        String displayName = StringUtil.colorCode("&7");
        if (player.isOp()) {
            displayName = StringUtil.colorCode("&5&l");
        } else if (player.hasPermission("color.owner")) {
            displayName = StringUtil.colorCode("&c&l");
        } else if (player.hasPermission("color.admin")) {
            displayName = StringUtil.colorCode("&4&l");
        } else if (player.hasPermission("color.mod")) {
            displayName = StringUtil.colorCode("&3&l");
        } else if (player.hasPermission("color.watcher")) {
            displayName = StringUtil.colorCode("&d&l");
        } else if (player.hasPermission("color.vip")) {
            displayName = StringUtil.colorCode("&a");
        }
        team.setPrefix(StringUtil.colorCode(statusEnums.getDisplayName() + "" + displayName));
        team.addEntry(player.getName());

    }
}
