package com.rednetty.status.mechanics.menus;

import com.rednetty.status.mechanics.Mechanic;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.HashMap;

public class MenuHandler extends Mechanic implements Listener {

    private HashMap<Player, MenuPlayer> menuPlayerHashMap = new HashMap<>();

    public void onEnable() {
        listener(this);
    }

    public void onDisable() {

    }


    /**
     * This is to get a MenuPlayer from a Bukkit Player type.
     * Menu Players are so the menu system can work more efficiently.*/
    public MenuPlayer getMenuPlayer(Player player) {
        if(!menuPlayerHashMap.containsKey(player)) return null;
        return menuPlayerHashMap.get(player);
    }

    /**
     * Creates a entry in the hashmap so the player has a linked MenuPlayer*/
    @EventHandler
    public void onLogin(PlayerJoinEvent event) {
        if(menuPlayerHashMap.containsKey(menuPlayerHashMap)) return;
        MenuPlayer menuPlayer = new MenuPlayer(event.getPlayer());
        menuPlayerHashMap.put(event.getPlayer(), menuPlayer);
    }


    @EventHandler
    public void onInteract(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (!getMenuPlayer(player).viewingMenu()) return;
        getMenuPlayer(player).getOpenMenu().handleClick(event.getRawSlot(), getMenuPlayer(player), event);
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        Player player = (Player) event.getPlayer();
        if (getMenuPlayer(player) == null) return;
        if (!getMenuPlayer(player).viewingMenu()) return;
        getMenuPlayer(player).closeMenu(false);
    }
}