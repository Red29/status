package com.rednetty.status.mechanics;

import com.rednetty.status.mechanics.status.StatusMechanic;
import com.rednetty.status.mechanics.menus.MenuHandler;

import java.util.stream.Stream;

public class MechanicHandler {

    private MenuHandler menuHandler;
    private StatusMechanic statusMechanic;

    /**
     * Initializes Classes using the Mechanic system
     */
    public void init() {
        Stream.of(
                menuHandler = new MenuHandler(),
                statusMechanic = new StatusMechanic()
        ).forEach(mechanic -> mechanic.onEnable());
    }

    /**
     * Disables all Classes using the Mechanic system
     */
    public void stop() {
        Stream.of(
                menuHandler,
                statusMechanic
        ).forEach(mechanic -> mechanic.onDisable());
    }


    public MenuHandler getMenuHandler() {
        return menuHandler;
    }

    public StatusMechanic getStatusMechanic() {
        return statusMechanic;
    }
}
